//
//  DashboardControllerViewModel+Delegate.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DashboardControllerViewModel : DashboardControllerUCDelegate
{
    func on(items: [Episode]) {
        self.items = items
        self.delegate?.showProgress(show: items.count == 0)
    }
}
