//
//  DashboardControllerVM.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

protocol DashboardControllerVM
{
    func onAppear()
    func pulse(row: Int) -> String
    func date(row: Int) -> String
    func type(row: Int) -> String
    func user(row: Int) -> String
    func set(type: EpisodeType?)
    
    var nbrOfRows : Int { get }
    var delegate : DashboardControllerVMDelegate? { get set }
}
