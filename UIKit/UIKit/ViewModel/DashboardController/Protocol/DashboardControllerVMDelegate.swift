//
//  DashboardControllerVMDelegate.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

protocol DashboardControllerVMDelegate
{
    func refreshTableView()
    func showProgress(show : Bool);
}
