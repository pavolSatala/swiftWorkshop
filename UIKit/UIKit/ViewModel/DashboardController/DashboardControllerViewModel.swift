//
//  DashboardControllerViewModel.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

class DashboardControllerViewModel : DashboardControllerVM
{
    var delegate: DashboardControllerVMDelegate?
    
    internal var useCases   : DashboardControllerUC = DI.dashboardControllerUseCases
    
    internal var items      : [Episode] = []
    {
        didSet {
            self.delegate?.refreshTableView()
        }
    }
    
    // Protocol members
    var nbrOfRows: Int {
        get {
            return self.items.count
        }
    }
    
    func onAppear() {
        self.useCases.delegate = self
        self.useCases.onControllerAppeared()
    }
    
    func pulse(row: Int) -> String {
        return String(format: "%.2f", self.items[row].avgPulse ?? 0.0)
    }
    
    func date(row: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
         
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: self.items[row].date ?? Date()) // Jan 2, 2001
    }
    
    func user(row: Int) -> String {
        return "\(self.useCases.getUser())"
    }
    
    func type(row: Int) -> String {
        return self.items[row].type?.rawValue ?? ""
    }
    
    func set(type: EpisodeType?) {
        self.useCases.onTypeSelected(type: type)
    }
    
}
