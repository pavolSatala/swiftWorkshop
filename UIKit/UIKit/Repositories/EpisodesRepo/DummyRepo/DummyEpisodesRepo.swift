//
//  DummyEpisodesRepo.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

class DummyEpisodesRepo : EpisodesRepoProtocol
{
    internal var delegate : EpisodesRepoDelegateProtocol?
    internal var timer : Timer?
    internal let NBR_DUMMY_USER  = 5
    
    func getEpisodes(delegate: EpisodesRepoDelegateProtocol) {
        self.delegate = delegate;
        self.downloadEpisode()
    }
    
    func startEpisodesListening(delegate: EpisodesRepoDelegateProtocol) {
        self.delegate = delegate;
        self.downloadEpisode()
        self.timer = Timer.scheduledTimer(timeInterval: 23.0, target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
    }
    
    func stopEpisodesListening() {
        self.delegate = nil
        self.timer?.invalidate()
    }
    
    @objc func timerTick()
    {
        self.downloadEpisode()
    }
}
