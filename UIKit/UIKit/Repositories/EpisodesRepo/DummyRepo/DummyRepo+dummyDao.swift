//
//  DummyRepo+dummyDao.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DummyEpisodesRepo
{
    internal func downloadEpisode() {
        let delay = Int.random(in: 1..<5) 
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(delay)) {
            self.sendDummyEpisodes()
        }
    }
   
}
