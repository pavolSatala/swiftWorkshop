//
//  DummyRepo+dumyData.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DummyEpisodesRepo
{
    internal func sendDummyEpisodes()
    {
        var episodes = [Episode]()
        
        for _ in 0 ..< Int.random(in: 750..<1000) {
            episodes.append(self.getRandomEpisode())
        }
        
        self.delegate?.onEpisodes(episodes)
    }
    
    private func getRandomEpisode() -> Episode
    {
        let ep = Episode()
        ep.avgPulse = Float.random(in: 0..<200)
        ep.date = Date.randomBetween(start: "2022-01-01", end: "2022-05-01")
        ep.type = getType(pulse: ep.avgPulse ?? 0.0)
        
        ep.userId = Int.random(in: 0..<NBR_DUMMY_USER)
        
        return ep
    }
    
    private func getType(pulse : Float) -> EpisodeType
    {
        if pulse < 30 {
            return .failure
        }
        if pulse < 80 {
            return .bradycardia
        }
        if pulse < 150 {
            return .normal
        }
        return .tachycardia
    }
}
