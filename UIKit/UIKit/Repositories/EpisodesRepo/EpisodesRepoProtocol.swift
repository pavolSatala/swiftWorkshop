//
//  EpisodesRepoProtocol.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

// get list of the all recorded episodes
protocol EpisodesRepoProtocol
{
    func getEpisodes(delegate: EpisodesRepoDelegateProtocol)
    func startEpisodesListening(delegate: EpisodesRepoDelegateProtocol)
    func stopEpisodesListening()
}
