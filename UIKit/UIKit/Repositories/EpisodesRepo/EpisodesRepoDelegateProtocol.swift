//
//  EpisodesRepoDelegateProtocol.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

protocol EpisodesRepoDelegateProtocol
{
    func onEpisodes(_: [Episode])
}
