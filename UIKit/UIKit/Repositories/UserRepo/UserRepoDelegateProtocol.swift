//
//  UserRepoDelegateProtocol.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

// return user id of the active user
protocol UserRepoDelegateProtocol
{
    func onUser(_: Int)
}
