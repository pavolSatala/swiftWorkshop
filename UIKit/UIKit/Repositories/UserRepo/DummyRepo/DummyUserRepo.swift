//
//  DummyUserRepo.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

class DummyUserRepo : UserRepoProtocol
{
    internal var delegate : UserRepoDelegateProtocol?
    internal var timer : Timer?
    internal let NBR_DUMMY_USER  = 5
    
    func getUser(delegate: UserRepoDelegateProtocol) {
        self.delegate = delegate;
        self.downloadUser()
    }
    
    func startUserListening(delegate: UserRepoDelegateProtocol) {
        self.delegate = delegate;
        self.downloadUser()
        self.timer = Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(timerTick), userInfo: nil, repeats: true)
    }
    
    func stopUserListening() {
        self.delegate = nil
        self.timer?.invalidate()
    }
    
    @objc func timerTick()
    {
        self.downloadUser()
    }
}
