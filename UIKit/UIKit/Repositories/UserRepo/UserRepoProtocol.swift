//
//  UserRepoProtocol.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

protocol UserRepoProtocol
{
    func getUser(delegate: UserRepoDelegateProtocol)
    func startUserListening(delegate: UserRepoDelegateProtocol)
    func stopUserListening()
}
