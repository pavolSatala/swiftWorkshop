//
//  DashboardCell.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import UIKit

class DashboardCell : UITableViewCell
{
    @IBOutlet var type : UILabel?
    @IBOutlet var pulse : UILabel?
    @IBOutlet var date : UILabel?
    @IBOutlet var user : UILabel?
}
