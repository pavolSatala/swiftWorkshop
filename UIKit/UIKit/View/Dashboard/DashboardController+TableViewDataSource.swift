//
//  Dashboard+TableViewDataSource.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation
import UIKit

extension DashboardController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.nbrOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "baseCell", for: indexPath) as? DashboardCell
        
        if cell != nil {
            cell?.pulse?.text = self.viewModel.pulse(row: indexPath.row)
            cell?.date?.text = self.viewModel.date(row: indexPath.row)
            cell?.type?.text = self.viewModel.type(row: indexPath.row)
            cell?.user?.text = self.viewModel.user(row: indexPath.row)
        }
        
        return cell ?? UITableViewCell()
    }
    
    
    
}
