//
//  Dashboard.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import UIKit

class DashboardController: UIViewController {

    internal var viewModel:  DashboardControllerVM = DI.dashboardControllerViewModel
    
    @IBOutlet var tableView : UITableView?
    @IBOutlet var progressView : UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.onAppear()
        self.viewModel.delegate = self
    }
    
    @IBAction func setAll()
    {
        self.viewModel.set(type: nil)
    }
    
    @IBAction func setBrady()
    {
        self.viewModel.set(type: .bradycardia)
    }
    
    @IBAction func setTachy()
    {
        self.viewModel.set(type: .tachycardia)
    }
    
    @IBAction func setFailure()
    {
        self.viewModel.set(type: .failure)
    }
    
    @IBAction func setNormal()
    {
        self.viewModel.set(type: .normal)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
