//
//  DashboardController+Delegate.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DashboardController : DashboardControllerVMDelegate
{
    func refreshTableView() {
        self.tableView?.reloadData()
    }
    
    func showProgress(show : Bool) {
        self.progressView?.isHidden = !show
    }
}
