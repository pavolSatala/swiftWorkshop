//
//  DI.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

// pre tieto učely zjednodušene, odporučam pozrieť Router z Apple Viper architektury
class DI
{
    static var dashboardControllerViewModel     = DashboardControllerViewModel();
    static var dashboardControllerUseCases      = DashboardControllerUseCases();
    static var episodesRepo     = DummyEpisodesRepo()
    static var userRepo         = DummyUserRepo()
}
