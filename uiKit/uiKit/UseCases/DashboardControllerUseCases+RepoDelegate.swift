//
//  DashboardControllerUseCases+RepoDelegate.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DashboardControllerUseCases : EpisodesRepoDelegateProtocol
{
    func onEpisodes(_ list : [Episode]) {
        self.items = list
    }
}
