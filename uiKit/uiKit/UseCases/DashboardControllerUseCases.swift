//
//  DashboardControllerUseCAses.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

class DashboardControllerUseCases : DashboardControllerUC
{
    
    internal var episodesRepo : EpisodesRepoProtocol = DI.episodesRepo
    internal var userRepo : UserRepoProtocol = DI.userRepo
    
    internal var items  : [Episode]?
    {
        didSet {
            self.filter()
        }
    }
    
    internal var user   : Int?
    {
        didSet {
            self.filter()
        }
    }
    
    internal var type   : EpisodeType?
    {
        didSet {
            self.filter()
        }
    }
    
    var delegate : DashboardControllerUCDelegate?
    
    func onControllerAppeared() {
        self.episodesRepo.startEpisodesListening(delegate: self)
        self.userRepo.startUserListening(delegate: self)
    }
    
    func onTypeSelected(type: EpisodeType?) {
        self.type = type
    }
    
    func getUser() -> Int
    {
        return user ?? 0
    }
    
    private func filter()
    {
        let episodes = items?.filter({ episode in
            let t = type;
            let ta = episode.type
            return episode.userId == user && (type == nil || episode.type == type)
        })
        self.delegate?.on(items: episodes ?? [Episode]())
    }
}
