//
//  DashboardControllerUV.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

protocol DashboardControllerUC
{
    func onControllerAppeared()
    func onTypeSelected(type: EpisodeType?)
    func getUser() -> Int
    
    var delegate : DashboardControllerUCDelegate? { get set }
   
}
