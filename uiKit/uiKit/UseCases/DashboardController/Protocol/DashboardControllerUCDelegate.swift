//
//  DashboardControllerUCDelegate.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

protocol DashboardControllerUCDelegate
{
    func on(items: [Episode])
}
