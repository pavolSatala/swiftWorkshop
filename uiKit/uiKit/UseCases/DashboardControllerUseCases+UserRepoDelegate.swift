//
//  DashboardControllerUseCases+UserRepoDelegate.swift
//  uiKit
//
//  Created by Pavol Satala on 4/8/22.
//

import Foundation

extension DashboardControllerUseCases : UserRepoDelegateProtocol
{
    func onUser(_ user: Int) {
        self.user = user
    }
}
